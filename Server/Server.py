from flask import Flask, jsonify, request
import hmac , hashlib


app = Flask(__name__)

#Almacenamiento en memoria
messages = [
    {
        'id': 1,
        'msg': 'El primero de muchos',
        'tag': 'Firt',
    }
]

credentials = [
    {
        'id': 1,
        'key': 'clave1',
        'shared_secret': 'top secret'
    }
]

#PUT de Credenciales
@app.route('/credential', methods=['PUT'])
def credential():

    #Validación de parametros recibidos
    if not request.json or not 'key' in request.json or not 'shared_secret' in request.json:
        abort(400)

    #Validación de existencia en el servidor
    cred = [cred for cred in credentials if cred['key'] == request.json['key']]
    #Creación de objeto para almacenamiento
    if len(cred) == 0:

        credential = {
                'id': credentials[-1]['id'] + 1,
                'key': request.json['key'],
                'shared_secret': request.json['shared_secret']
            }

        credentials.append(credential)

        return jsonify({'Credential': credential}), 204

    # Key existe.
    if len(cred) != 0:
        return ('Credential encontrada'),403

def verify(request,shared_secret):

    body = request.get_data()
    route = request.headers['X-Route']
    sig_basestring = 'v0;%s;%s' % (route, body.decode('utf-8'))
    computed_sha = hmac.new(shared_secret.encode('utf-8'),
                            sig_basestring.encode('utf-8'),
                            digestmod=hashlib.sha256).hexdigest()
    my_sig = 'v0=%s' % (computed_sha,)
    signature = request.headers['X-Signature']
    if my_sig != signature:
        return 1



#POST Mensaje
@app.route('/message', methods=['POST'])
def create_message():

    if not request.headers or not 'X-Key' in request.headers or not 'X-Route' in request.headers or not 'X-Signature' in request.headers:
        return jsonify('noheader'),400

    #Validación de parametros recibidos
    if not request.json or not 'msg' in request.json or not 'tag' in request.json:
        abort(400)

    cred = [cred for cred in credentials if cred['key'] == request.headers['X-Key']]

    if len(cred) != 0:
        ver=verify(request, cred[0]['shared_secret'])
        #Creación de objeto para almacenamiento
        if ver != 1:
            message = {
                'id': messages[-1]['id'] + 1,
                'msg': request.json['msg'],
                'tag': request.json['tag']
            }

            #Almacenamiento de objeto
            messages.append(message)
            return jsonify({'message': message}), 200
        if ver == 1:
            return jsonify('Firma desigual'),403
#Solo validacion de todos los mensajes
@app.route('/todo', methods=['GET'])
def get_tasks():
    return jsonify({'Messages': messages})

#GET Message ID
@app.route('/message/<int:id>', methods=['GET'])
def get_messageid(id):

    if not request.headers or not 'X-Key' in request.headers or not 'X-Route' in request.headers or not 'X-Signature' in request.headers:
        return jsonify('noheader'),400

    cred = [cred for cred in credentials if cred['key'] == request.headers['X-Key']]

    if len(cred) != 0:
        ver=verify(request, cred[0]['shared_secret'])
        if ver != 1:
        #Busqueda de mensaje según id
            msg = [msg for msg in messages if msg['id'] == id]

            if len(msg) == 0:
                abort(404)

            return jsonify({'message':msg[0]}), 200
        if ver == 1:
            return jsonify('Firma desigual'),403

#GET Message por tag
@app.route('/message/<string:tag>', methods=['GET'])
def get_messagetag(tag):
    if not request.headers or not 'X-Key' in request.headers or not 'X-Route' in request.headers or not 'X-Signature' in request.headers:
        return jsonify('noheader'),400

    cred = [cred for cred in credentials if cred['key'] == request.headers['X-Key']]

    if len(cred) != 0:
        ver=verify(request, cred[0]['shared_secret'])
        if ver != 1:
            msg = [msg for msg in messages if msg['tag'] == tag]

            if len(msg) == 0:
                abort(404)

            return jsonify(msg), 200
        if ver == 1:
            return jsonify('Firma desigual'),403

if __name__ == '__main__':
    app.run(debug=True)
